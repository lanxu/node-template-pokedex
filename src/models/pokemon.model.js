import Sequelize from 'sequelize';
import sequelize from '../sequelize';

const schema = sequelize.define('pokemon', {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  type: Sequelize.STRING,
  type1: Sequelize.INTEGER,
  type2: Sequelize.INTEGER,
  name: Sequelize.STRING,
  level: Sequelize.INTEGER,
  gender: Sequelize.INTEGER,
  previous_form_id: {type: Sequelize.INTEGER, foreignKey: true},
});

export default schema;
