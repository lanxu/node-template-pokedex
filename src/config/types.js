const Types = [
  {
    id: 0,
    name: 'Normal',
  },
  {
    id: 1,
    name: 'Fire',
  },
  {
    id: 2,
    name: 'Fighting',
  },
  {
    id: 3,
    name: 'Water',
  },
  {
    id: 4,
    name: 'Flying',
  },
  {
    id: 5,
    name: 'Grass',
  },
  {
    id: 6,
    name: 'Poison',
  },
  {
    id: 7,
    name: 'Electric',
  },
  {
    id: 8,
    name: 'Ground',
  },
  {
    id: 9,
    name: 'Psychic',
  },
  {
    id: 10,
    name: 'Rock',
  },
  {
    id: 11,
    name: 'Ice',
  },
  {
    id: 12,
    name: 'Bug',
  },
  {
    id: 13,
    name: 'Dragon',
  },
  {
    id: 14,
    name: 'Ghost',
  },
  {
    id: 15,
    name: 'Dark',
  },
  {
    id: 16,
    name: 'Steel',
  },
  {
    id: 17,
    name: 'Fairy',
  },
  {
    id: 18,
    name: 'None',
  },
];

export default Types;
