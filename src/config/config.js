import process from 'process';

// This is an example. environment constant describes the default variables
// which can be overridden by the real environment variables

/**
 * checkEnv checks from environment variable
 *
 * @param {string} name variable name
 * @returns {boolean} if variable exists
 */
function checkEnv(name) {
  let variable = process.env[name];

  if(typeof variable === 'undefined' || variable === '') {
    return false;
  }

  return true;
}

const environment = {
  NODE_ENV: process.NODE_ENV,
  HOST: checkEnv('HOST') ? process.env.HOST : 'localhost',
  PORT: checkEnv('PORT') ? process.env.PORT : 8080,
  SQL_HOST: checkEnv('SQL_HOST') ? process.env.SQL_HOST : 'localhost',
  SQL_PORT: checkEnv('SQL_PORT') ? process.env.SQL_PORT : 5432,
  SQL_DATABASE: checkEnv('SQL_DATABASE') ? process.env.SQL_DATABASE : 'nodetemplate',
  SQL_USERNAME: checkEnv('SQL_USERNAME') ? process.env.SQL_USERNAME : 'lanxu',
  SQL_PASSWORD: checkEnv('SQL_PASSWORD') ? process.env.SQL_PASSWORD : '1q2w3e',
};

const config = {
  env: environment.NODE_ENV,
  host: environment.HOST,
  port: environment.PORT,
  sql: {
    database: environment.SQL_DATABASE,
    username: environment.SQL_USERNAME,
    password: environment.SQL_PASSWORD,
    host: environment.SQL_HOST,
    port: environment.SQL_PORT,
  },
};

export default config;
