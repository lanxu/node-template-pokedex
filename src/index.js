import config from './config/config';
import app from './express';
import sequelize from './sequelize';
import logger from './logger';

// Register models

// Sync. Create models if needed
sequelize.sync();

// Start app
app.listen(config.port, () => {
  logger.info('Server started on ' + config.host + ':' + config.port);
});

export default app;
