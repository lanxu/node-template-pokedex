import express from 'express';
import pokemonRoutes from './pokemon';
import typesRoutes from './types';

const router = express.Router();

router.use('/pokemons', pokemonRoutes);
router.use('/types', typesRoutes);
// Add here the rest of the availble CRUD apis

export default router;
