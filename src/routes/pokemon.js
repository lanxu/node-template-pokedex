import express from 'express';
import pokemonController from '../controllers/pokemon.controller';

const router = express.Router();

router.route('/')
  .get(pokemonController.list)
  .post(pokemonController.create);

router.route('/:id')
  .get(pokemonController.get)
  .put(pokemonController.update)
  .delete(pokemonController.remove);

router.param('id', pokemonController.load);

export default router;
