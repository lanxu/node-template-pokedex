import express from 'express';
import typesController from '../controllers/types.controller';

const router = express.Router();

router.route('/')
  .get(typesController.list)
  .post(typesController.create);

router.route('/:id')
  .get(typesController.get)
  .put(typesController.update)
  .delete(typesController.remove);

router.param('id', typesController.load);

export default router;
