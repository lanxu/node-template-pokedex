import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import expressValidator from 'express-validator';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import routes from './routes/index';
import logger from './logger';
import morgan from 'morgan';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());

app.use(cookieParser());
app.use(compress());
app.use(helmet());
app.use(cors());

app.use(morgan('combined', {stream: logger.stream}));

app.use('/', express.static(__dirname + '/public'));
app.use('/api', routes);

export default app;
