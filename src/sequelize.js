import config from './config/config';
import Sequelize from 'sequelize';
import pg from 'pg';
import logger from './logger';

const sequelize = new Sequelize(config.sql.database, config.sql.username, config.sql.password, {
  host: config.sql.host,
  dialect: 'postgres',
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

sequelize
  .authenticate()
  .then(() => {
    logger.info('Connected to the database');
  })
  .catch(() => {
    logger.info('Connection to the database failed. Attempting to create database');

    // Create the database (the most common failure)
    let connectionString = 'postgres://' + config.sql.username + ':' + config.sql.password + '@' + config.sql.host + ':' + config.sql.port + '/postgres';

    logger.info('Using connection string: ' + connectionString);

    let pool = new pg.Pool({
      user: config.sql.username,
      host: config.sql.host,
      database: 'postgres',
      password: config.sql.password,
      port: config.sql.port,
    });

    pool.query('CREATE DATABASE ' + config.sql.database);
  });

export default sequelize;

