import Types from '../config/types';
import logger from '../logger';

function load(req, res, next, id) {
  return res.json(Types.find((type) => {
    return type.id === id;
  }));
}

function get(req, res) {
  return res.json(Types);
}

function create() {
  logger.info('not supported!');
}

function update() {
  logger.info('not supported');
}

function list(req, res, next) {
  res.json(Types);
}

function remove() {
  logger.info('not supported');
}


export default {load, get, create, update, list, remove};
