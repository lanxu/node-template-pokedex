import Pokemon from '../models/pokemon.model';
import logger from '../logger';

function validatePokemon(pokemon) {
  if(typeof pokemon === 'undefined' || pokemon === null) {
    return;
  }

  if(pokemon.level < 1) {
    logger.debug('Pokemon less than level 1');
    pokemon.level = 1;
  }

  if(pokemon.level > 100) {
    logger.debug('Pokemon over level 100');
    pokemon.level = 100;
  }

  return pokemon;
}

/**
 * load is used to fetch the document for id based queries
 */
function load(req, res, next, id) {
  Pokemon.findById(id).then((pokemon) => {
    req.pokemon = pokemon;
    return next();
  });
}

/**
 * get a single document (by id)
 */
function get(req, res) {
  return res.json(req.pokemon);
}

/**
 * create a single document
 */
function create(req, res, next) {
  // validate pokemon before adding it
  let newPokemon = validatePokemon(req.body);

  Pokemon.create(newPokemon).then(() => {
    return Pokemon.findOrCreate({where: {name: newPokemon.name}})
      .spread((pokemon, created) => {
        logger.debug(pokemon.get({plain: true}));
        res.json(pokemon.get());
      }).catch((e) => next(e));
  }).catch((e) => next(e));
}

/**
 * update a single document (by id)
 */
function update(req, res, next) {
  // validate pokemon before adding it
  let newPokemon = validatePokemon(req.body);
  Pokemon.update(newPokemon, {where: {id: newPokemon.id}}).then(() => {
    return Pokemon.findOrCreate({where: {id: req.body.id}})
      .spread((pokemon, created) => {
        logger.debug(pokemon.get({plain: true}));
        res.json(pokemon.get());
      }).catch((e) => next(e));
  }).catch((e) => next(e));
}

/**
 * list documents from the database. by default limited to 50.
 */
function list(req, res, next) {
  const {limit = 50, skip = 0} = req.query;
  Pokemon.findAndCountAll({
    offset: skip,
    limit: limit,
  }).then((rows) => res.json(rows));
}

/*
 * remove documents from the database (by id)
 */
function remove(req, res, next) {
  if(req.pokemon !== null) {
    req.pokemon.destroy();
  }
}

/*
 * export the functionality
 */
export default {load, get, create, update, list, remove};
