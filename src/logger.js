import fs from 'fs';
import winston from 'winston';

const logDirectory = __dirname + '/logs/';

// ensure log directory exists
if(!fs.existsSync(logDirectory)) {
  fs.mkdirSync(logDirectory);
}

let logger = new winston.Logger({
  level: process.env.NODE_ENV !== 'production' ? 'error' : 'debug',
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: logDirectory + '/combined.log',
      handleExceptions: true,
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
      timestamp: true,
    }),
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true,
    }),
  ],
  exceptionHandlers: [
    new winston.transports.Console({
      json: false,
      timestamp: true,
    }),
    new winston.transports.File({
      filename: logDirectory + '/exceptions.log',
      json: false,
    }),
  ],
  exitOnError: false,
});

logger.stream = {
  write: function(message, encoding) {
    logger.info(message);
  },
};

export default logger;
