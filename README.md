# node-template-pokedex

This is a node/express template to demonstrate the project configuration.

## Requirements

- Node.js
- Yarn (or npm)
- Postgresql (6.x+)
- ESLint recommended

## Setup database

Make sure you have Postgresql installed and an initial user with database created. You can edit src/config/config.js or you may modify the environment variables to suit your needs. Available environment variables are:

| Variable     | Explanation       |
|--------------|-------------------|
| HOST         | IP to bind to     |
| PORT         | Port used         |
| SQL_HOST     | SQL host          |
| SQL_PORT     | SQL port          |
| SQL_DATABASE | Database name     |
| SQL_USERNAME | Database username |
| SQL_PASSWORD | Database password |


## Setup application

```sh
git clone https://lanxu@bitbucket.org/lanxu/node-template-pokedex.git
yarn install
npm run dev
```

## Frontend

Available by default at http://localhost:8080/

## Author

- Jukka Lankinen <jukka.lankinen@gmail.com>
